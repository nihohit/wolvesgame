﻿using UnityEngine;

public class WolfInformation {
    public GameObject wolf;
    public int score;
    public Vector2Int location;
}
