﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MapScript : MonoBehaviour {
  private readonly int stepsInTurn = 5;

  #region public variables
  public GameObject tilePrefab;
  public GameObject wolfPrefab;
  public GameObject rabbitPrefab;
  public GameObject deerPrefab;
  public GameObject boarPrefab;
  public GameObject stepsLeftTextBox;
  public GameObject scoreTextBox;
  public GameObject wolfNameTextBox;
  public GameObject starvationTextBox;

  public GameObject regularCanvas;
  public GameObject winningCanvas;
  public GameObject winningText;

  public Vector2Int size;
  #endregion

  #region private variables
  private System.Random random = new System.Random();
  private List<Renderer> treeRenderers;
  private List<Collider> treeColliders;
  private Vector3 tileSize;
  private int currentTakenSteps;
  private Dictionary<Vector2Int, TileScript> tileMap;
  private Dictionary<Vector2Int, GameObject> preyTable;
  private WolfInformation wolf1;
  private WolfInformation wolf2;
  private WolfInformation currentWolf;
  private Vector3 cameraOffset;

  private AudioClip step1;
  private AudioClip step2;
  private AudioClip step3;
  private AudioClip step4;
  private AudioClip rabbitStep;
  private AudioClip howl;
  private AudioClip eat;
  private bool inputEnabled;
  private int turnsUntilStarvation = 20;
  private bool shouldAdjustTreeVisibility;
  #endregion

  private void Update() {
    Camera.main.transform.position = currentWolf.wolf.transform.position + cameraOffset;
    if (shouldAdjustTreeVisibility) {
      foreach (var treeCollider in treeColliders) {
        treeCollider.enabled = true;
      }

      var seenTiles = tileMap
        .Where(pair => isSeen(currentWolf.location, pair.Key))
        .Select(pair => pair.Value)
        .ToList();
      foreach (var tile in seenTiles) {
        var hits = Physics.RaycastAll(Camera.main.transform.position, tile.transform.position - Camera.main.transform.position);
        foreach (var treeHit in hits.Where(hit => hit.collider.gameObject.tag.Equals("Tree"))) {
          changeTransparency(treeHit.collider.gameObject.GetComponent<Renderer>(), 0.1f);
        }
      }

      foreach (var treeCollider in treeColliders) {
        treeCollider.enabled = false;
      }
      shouldAdjustTreeVisibility = false;
    }
    if (Input.GetKeyDown(KeyCode.Escape)) {
      Application.Quit();
    }
  }

  #region initialization

  // Use this for initialization
  void Start() {
    regularCanvas.SetActive(true);
    winningCanvas.SetActive(false);
    loadSounds();
    createTiles();
    setTreeRenderers();
    createGrid();
    createWolves();
    createPrey();
    switchWolves();
    setScore();
    inputEnabled = true;
    starvationTextBox.GetComponent<Text>().text = "Turns until starvation " + turnsUntilStarvation;
  }

  void loadSounds() {
    step1 = Resources.Load<AudioClip>("Sounds/steps_1");
    step2 = Resources.Load<AudioClip>("Sounds/steps_2");
    step3 = Resources.Load<AudioClip>("Sounds/steps_3");
    step4 = Resources.Load<AudioClip>("Sounds/steps_4");
    rabbitStep = Resources.Load<AudioClip>("Sounds/steps_bunny");
    howl = Resources.Load<AudioClip>("Sounds/howl");
    eat = Resources.Load<AudioClip>("Sounds/eat");
  }

  void createTiles() {
    var extraHeightVector = new Vector3(0, 0.1f, 0);
    var tiles = new GameObject();
    tiles.name = "tiles";
    tileMap = new Dictionary<Vector2Int, TileScript>();
    for (int i = -size.x; i < size.x; i++) {
      for (int j = -size.y; j < size.y; j++) {
        var newTile = Instantiate(tilePrefab).GetComponent<TileScript>();

        newTile.map = this;
        newTile.coords = new Vector2Int(i, j);
        var bounds = newTile.GetComponent<BoxCollider>().bounds;
        tileSize = bounds.size;
        newTile.transform.position =
          centerOfTilePosition(new Vector2Int(i, j)) + extraHeightVector;
        newTile.name = string.Format("Tile {0}.{1}", i, j);
        tileMap[new Vector2Int(i, j)] = newTile;
        newTile.transform.parent = tiles.transform;
      }
    }
  }

  void createGrid() {
    float lineHeightOffset = 0.1f;

    var grid = new GameObject();
    grid.name = "grid";

    var xLength = size.x * tileSize.x;
    var yLength = size.y * tileSize.z;

    for (int i = -size.x; i <= size.x; i++) {
      createGridLine(new Vector3(i * tileSize.x, lineHeightOffset, -yLength),
              new Vector3(i * tileSize.x, lineHeightOffset, yLength),
              grid);
    }

    for (int i = -size.y; i <= size.y; i++) {
      createGridLine(new Vector3(-xLength, lineHeightOffset, i * tileSize.z),
              new Vector3(xLength, lineHeightOffset, i * tileSize.z),
              grid);
    }
  }

  void createGridLine(Vector3 start, Vector3 end, GameObject parent) {
    var line = new GameObject();
    var lineScript = line.AddComponent<LineRenderer>();
    lineScript.material = new Material(Shader.Find("Diffuse"));
    lineScript.startColor = Color.black;
    lineScript.endColor = Color.black;

    lineScript.SetPositions(new Vector3[] { start, end });
    line.name = string.Format("{0}.{1}", start, end);
    line.transform.parent = parent.transform;
  }

  void createWolves() {
    wolf1 = createWolfInformation(Vector2Int.zero - size);
    cameraOffset = Camera.main.transform.position - wolf1.wolf.transform.position;
    Camera.main.transform.LookAt(wolf1.wolf.transform);
    wolf1.wolf.name = "wolf1";
    wolf2 = createWolfInformation(MapInfo.competitiveGame ? size - new Vector2Int(1, 1) :
                                  new Vector2Int(1, 1) - size);

    wolf2.wolf.name = "wolf2";
    updateSteps(0);
  }

  WolfInformation createWolfInformation(Vector2Int location) {
    var wolfInfo = new WolfInformation();
    wolfInfo.wolf = createWolf(location);
    wolfInfo.location = location;
    wolfInfo.score = 0;
    return wolfInfo;
  }

  GameObject createWolf(Vector2Int location) {
    var wolf = Instantiate(wolfPrefab);
    placeAnimal(wolf, location);
    return wolf;
  }

  void createPrey() {
    int initialNumberOfRabbitPods = 6;

    preyTable = new Dictionary<Vector2Int, GameObject>();
    for (int i = 0; i < initialNumberOfRabbitPods; i++) {
      createRabbitPod(i);
    }

    int initialNumberOfDeers = 3;
    for (int i = 0; i < initialNumberOfDeers; i++) {
      createDeer(i);
    }

    int initialNumberOfBoarPods = 3;
    for (int i = 0; i < initialNumberOfBoarPods; i++) {
      createBoarPod(i);
    }
  }

  private void createBoarPod(int podNumber) {
    int numberOfAnimalsInPod = random.Next(1, 2);
    var podCenter = new Vector2Int(random.Next(-size.x, size.x), random.Next(-size.y, size.y));

    for (int i = 0; i < numberOfAnimalsInPod; i++) {
      createAnimal(boarPrefab, string.Format("Boar {0} from pod {1}", i, podNumber),
             podCenter);
    }
  }

  void createRabbitPod(int podNumber) {
    int numberOfAnimalsInPod = random.Next(2, 3);
    var podCenter = new Vector2Int(random.Next(-size.x, size.x), random.Next(-size.y, size.y));

    for (int i = 0; i < numberOfAnimalsInPod; i++) {
      createAnimal(rabbitPrefab, string.Format("Rabbit {0} from pod {1}", i, podNumber),
             podCenter);
    }
  }

  void createDeer(int deerNumber) {
    var searchCenter = new Vector2Int(random.Next(-size.x, size.x), random.Next(-size.y, size.y));
    createAnimal(deerPrefab, string.Format("Deer {0}", deerNumber), searchCenter);
  }

  void createAnimal(GameObject prefab, string name, Vector2Int searchCenter) {
    var location = findEmptySpace(searchCenter);
    var animal = Instantiate(prefab);
    animal.name = name;
    var animalSize = animal.GetComponent<BoxCollider>().bounds.size;
    animal.transform.position = Vector3.up * animalSize.y;
    placeAnimal(animal, location);
    animal.GetComponent<BoxCollider>().enabled = false;
    preyTable[location] = animal;
  }

  Vector2Int findEmptySpace(Vector2Int searchCenter) {
    var location = Vector2Int.zero;
    while (!isEmpty(location) || !tileMap.ContainsKey(location)) {
      var offset = new Vector2Int(random.Next(-1, 1), random.Next(-1, 1));
      location = searchCenter + offset;
    }
    return location;
  }

  void setTreeRenderers() {
    treeRenderers = tileMap.Values.Select(tile => tile.tree.GetComponent<Renderer>()).ToList();
    treeColliders = tileMap.Values.Select(tile => tile.tree.GetComponent<Collider>()).ToList();
  }
  #endregion

  #region moving wolf
  public void ClickedOn(Vector2Int coords) {
    if (Vector2Int.Distance(coords, currentWolf.location) > 1 ||
        containsWolf(coords) ||
        currentTakenSteps == stepsInTurn ||
        !inputEnabled) {
      return;
    }

    checkAndEatPrey(coords);
    placeWolf(coords);
    propagateSound(coords, currentTakenSteps);
    playSound();
    takeStep();
  }

  private void checkAndEatPrey(Vector2Int coords) {
    GameObject preyInPoint;
    if (!preyTable.TryGetValue(coords, out preyInPoint)) {
      return;
    }
    Destroy(preyInPoint);
    preyTable.Remove(coords);
    var score = 1;
    if (preyInPoint.tag == "Deer") {
      score = 3;
    } else if (preyInPoint.tag == "Boar") {
      score = 2;
    }
    currentWolf.score += score;
    setScore();
    GetComponent<AudioSource>().PlayOneShot(eat);
  }

  void placeWolf(Vector2Int location) {
    inputEnabled = false;
    var tileCenter = centerOfTilePosition(location);
    currentWolf.wolf.transform.LookAt(new Vector3(tileCenter.x, currentWolf.wolf.transform.position.y, tileCenter.z));
    StartCoroutine(MoveOverSeconds(currentWolf.wolf,
      tileCenter + new Vector3(0, currentWolf.wolf.transform.position.y, 0), 0.2f,
      () => { inputEnabled = true; }));
    currentWolf.location = location;
    revealAreaAroundCurrentWolf();
  }

  private void placeAnimal(GameObject animal, Vector2Int coords) {
    var tileCenter = centerOfTilePosition(coords);
    animal.transform.LookAt(new Vector3(tileCenter.x, animal.transform.position.y, tileCenter.z));
    animal.transform.position = tileCenter + new Vector3(0, animal.transform.position.y, 0);
  }

  private Vector3 centerOfTilePosition(Vector2Int coords) {
    var halfTileSize = tileSize / 2;
    return new Vector3(coords.x * tileSize.x, 0, coords.y * tileSize.z) + halfTileSize;
  }

  private void playSound() {
    var audioSource = GetComponent<AudioSource>();
    switch (currentTakenSteps) {
      case 1:
        audioSource.PlayOneShot(step1);
        break;
      case 2:
        audioSource.PlayOneShot(step2);
        break;
      case 3:
        audioSource.PlayOneShot(step3);
        break;
      case 4:
        audioSource.PlayOneShot(step4);
        break;
    }
  }

  private void takeStep() {
    updateSteps(currentTakenSteps + 1);
  }

  private void revealAreaAroundCurrentWolf() {
    foreach (var pair in preyTable) {
      bool result = isSeen(currentWolf.location, pair.Key);
      foreach (var meshRenderer in pair.Value.GetComponentsInChildren<MeshRenderer>()) {
        meshRenderer.enabled = result;
      }
    }

    foreach (var treeRenderer in treeRenderers) {
      changeTransparency(treeRenderer, 1);
    }
    shouldAdjustTreeVisibility = true;
  }

  private static void changeTransparency(Renderer renderer, float opacity) {
    foreach (var material in renderer.materials) {
      var color = material.color;
      color.a = opacity;
      material.color = color;
    }
  }

  private bool isSeen(Vector2Int wolfLocation, Vector2Int locationToCheck) {
    const int kDistanceToSee = 3;
    return Vector2Int.Distance(wolfLocation, locationToCheck) < kDistanceToSee;
  }

  public void endTurn() {
    var tilesWithSound = tileMap
      .Where(pair => pair.Value.HasSound && preyTable.ContainsKey(pair.Key))
      .Select(pair => pair.Key).ToList();
    foreach (var tile in tileMap.Values) {
      tile.ResetVisuals();
    }
    bool anyPreyEscaped = false;
    foreach (var tileCoords in tilesWithSound) {
      GameObject preyInPoint;
      if (preyTable.TryGetValue(tileCoords, out preyInPoint)) {
        preyEscapes(preyInPoint, tileCoords, currentWolf.location);
        anyPreyEscaped = true;
      }
    }
    if (anyPreyEscaped) {
      GetComponent<AudioSource>().PlayOneShot(rabbitStep);
    }

    updateStarvationMeter();

    switchWolves();

    updateSteps(0);
  }

  private void updateStarvationMeter() {
    turnsUntilStarvation--;

    if (turnsUntilStarvation == 0) {
      winningText.GetComponent<Text>().text = "You lose :(";
      regularCanvas.SetActive(false);
      winningCanvas.SetActive(true);
    }

    starvationTextBox.GetComponent<Text>().text = "Turns until starvation " + turnsUntilStarvation;
  }

  private void updateSteps(int newStepValue) {
    currentTakenSteps = newStepValue;
    stepsLeftTextBox.GetComponent<Text>().text =
      string.Format("Steps remaining: {0}", stepsInTurn - newStepValue);
  }

  private void propagateSound(Vector2Int coords, int distance) {
    for (int i = -distance; i <= distance; i++) {
      for (int j = -distance; j <= distance; j++) {
        var location = new Vector2Int(i, j) + coords;
        TileScript tile;
        if (!tileMap.TryGetValue(location, out tile)) {
          continue;
        }
        tile.HasSound = true;
      }
    }
  }

  private void preyEscapes(GameObject preyInPoint, Vector2Int preyLocation,
               Vector2Int soundSource) {
    int escapeDistance = random.Next(1, 3);
    if (preyInPoint.tag == "Deer") {
      escapeDistance += 2;
    } else if (preyInPoint.tag == "Boar") {
      escapeDistance += 1;
    }
    var lastLocation = preyLocation;
    for (int i = 0; i < escapeDistance; i++) {
      TileScript tile;
      if (!tileMap.TryGetValue(lastLocation, out tile)) {
        return;
      }
      tile.HasPrints = true;
      var potentialLocations = randomOrderedOffsets()
        .Select(offset => lastLocation + offset)
        .Where(canEnter)
        .Where(location => Vector2Int.Distance(location, soundSource) >=
            Vector2Int.Distance(lastLocation, soundSource))
        .ToList();

      if (potentialLocations.Count == 0) {
        return;
      }
      var nextLocation = potentialLocations[0];
      placeAnimal(preyInPoint, nextLocation);
      preyTable.Remove(lastLocation);
      preyTable[nextLocation] = preyInPoint;
      lastLocation = nextLocation;
    }
  }

  private List<Vector2Int> randomOrderedOffsets() {
    var offsets = new List<Vector2Int> {
      new Vector2Int(1, 0),
      new Vector2Int(-1, 0),
      new Vector2Int(0, 1),
      new Vector2Int(0, 1)
    };
    int n = offsets.Count;
    while (n > 1) {
      n--;
      int k = random.Next(n + 1);
      var value = offsets[k];
      offsets[k] = offsets[n];
      offsets[n] = value;
    }
    return offsets;
  }

  private bool canEnter(Vector2Int location) {
    return isEmpty(location) && tileMap.ContainsKey(location);
  }

  public void Howl() {
    int howlDistance = 7;

    GetComponent<AudioSource>().PlayOneShot(howl);
    propagateSound(currentWolf.location, howlDistance);
  }

  public void FinishGame() {
    SceneManager.LoadScene("Start");
  }

  #endregion

  #region auxiliary methods
  private bool isEmpty(Vector2Int coords) {
    return !containsPrey(coords) && !containsWolf(coords);
  }

  private bool containsWolf(Vector2Int coords) {
    return wolf1.location.Equals(coords) || wolf2.location.Equals(coords);
  }

  private bool containsPrey(Vector2Int coords) {
    return preyTable.Keys.Any(location => location.Equals(coords));
  }

  private void setScore() {
    var score = MapInfo.competitiveGame ? currentWolf.score : wolf1.score + wolf2.score;
    scoreTextBox.GetComponent<Text>().text = string.Format("Score: {0}", score);

    if ((MapInfo.competitiveGame && score > 5) ||
        (!MapInfo.competitiveGame && score > 8)) {
      winningText.GetComponent<Text>().text = "You won!";
      regularCanvas.SetActive(false);
      winningCanvas.SetActive(true);
    }
  }

  void switchWolves() {
    currentWolf = currentWolf == wolf1 ? wolf2 : wolf1;
    setScore();
    wolfNameTextBox.GetComponent<Text>().text = currentWolf.wolf.name;
    revealAreaAroundCurrentWolf();
  }

  public IEnumerator MoveOverSeconds(GameObject objectToMove, Vector3 end, float seconds, Action continuation) {
    float elapsedTime = 0;
    Vector3 startingPos = objectToMove.transform.position;
    while (elapsedTime < seconds) {
      objectToMove.transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
      elapsedTime += Time.deltaTime;
      yield return new WaitForEndOfFrame();
    }
    objectToMove.transform.position = end;
    continuation();
  }

  #endregion
}
