﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {
  private Vector3 dragOrigin;
  private float dragSpeed = 30;

  // Use this for initialization
  void Start() {

  }

  // Update is called once per frame
  void Update() {
    if (Input.GetMouseButtonDown(1)) {
      dragOrigin = Input.mousePosition;
      return;
    }

    if (!Input.GetMouseButton(1)) return;

    Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
    Vector3 move = new Vector3(pos.x * dragSpeed, 0, pos.y * dragSpeed);

    transform.Translate(move, Space.World);
    dragOrigin = Input.mousePosition;
  }
}
