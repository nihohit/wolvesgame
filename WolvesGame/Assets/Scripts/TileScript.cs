﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileScript : MonoBehaviour {
  static private System.Random random = new System.Random();
  public GameObject tree;
  public MapScript map;
  public GameObject soundPropagationMarker;
  public GameObject pawPrintsMarker;
  public Vector2Int coords;

  private int pawPrintsAge;

  public bool HasSound {
    get {
      return soundPropagationMarker.activeSelf;
    }
    set {
      soundPropagationMarker.SetActive(value);
    }
  }

  public bool HasPrints {
    get {
      return pawPrintsMarker.activeSelf;
    }
    set {
      if (value) {
        pawPrintsAge = 2;
        pawPrintsMarker.SetActive(true);
      }
    }
  }

  public void ResetVisuals() {
    HasSound = false;
    pawPrintsAge--;

    if (pawPrintsAge <= 0) {
      pawPrintsAge = 0;
      pawPrintsMarker.SetActive(false);
    }
  }

  private void OnMouseDown() {
    map.ClickedOn(coords);
  }

  private void Awake() {
    chooseTree();
    ResetVisuals();
  }

  void chooseTree() {
    var treeIndex = random.Next(1, 30);
    var treeIndexStr = treeIndex > 9 ? treeIndex.ToString() : "0" + treeIndex;
    var treeName = "RegularTrees/tree0" + treeIndexStr;
    tree = Instantiate<GameObject>(Resources.Load<GameObject>(treeName));
    tree.transform.parent = transform;
    var xOffset = (float)random.NextDouble() * 6 - 3;
    var zOffset = (float)random.NextDouble() * 6 - 3;
    tree.transform.localPosition = new Vector3(xOffset, 0, zOffset);
    float scale = (float)random.NextDouble() * 1.5f + 1.5f;
    tree.transform.localScale = new Vector3(scale, scale, scale);
  }
}
