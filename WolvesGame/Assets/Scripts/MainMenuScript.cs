﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

  private void Update() {
    if (Input.GetKeyDown(KeyCode.Escape)) {
      Application.Quit();
    }
  }

  public void StartCompetitiveGame() {
    MapInfo.competitiveGame = true;
    SceneManager.LoadScene("map");
  }

  public void StartCooperativeGame() {
    MapInfo.competitiveGame = false;
    SceneManager.LoadScene("map");
  }
}
